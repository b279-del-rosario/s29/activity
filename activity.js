// Find Users with letter S and E in their first and last name
db.users.find(
	{ $or: [{firstName: {$regex: "S", $options: "i"}}, {lastName: {$regex: "D", $options: "i"}}]},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
);


// Users who are from HR department and age is greater or equal to 70


db.users.find({ $and: [{department: "HR"}, {age: {$gt: 70}}] });


// Find users with letter e in their first name and has an age of less than or equal to 30

db.users.find(
	{ $and: [{firstName: {$regex: "E", $options: "i"}}, {age: {$lte: 30}}]},
	{
		firstName: 1,
		lastName: 1,
		age: 1,
		contact: 1,
		courses: 1,
		department: 1
	}
);



